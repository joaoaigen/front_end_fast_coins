import React, {ChangeEvent, FormEvent, useState} from "react";
import '../styles/pages/login.css';
import api from "../services/api";
import logoImg from "../images/logo.svg";
import swal from 'sweetalert';
import {useHistory} from "react-router-dom";


export default function Login() {
    const history = useHistory();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [emailError, setEmailError] = useState();

    async function handleSubmit(event: FormEvent) {
        event.preventDefault();
        const data = new FormData();

        // @ts-ignore
        data.append('email', email);
        // @ts-ignore
        data.append('password', password);

        // @ts-ignore
        await api.post('login', {
            email,
            password
        }, {
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((response: any) => {
            localStorage.setItem('token', response.data.token);

            history.push('/app')
        }).catch((error: any) => {
            if (error.response.data.errors && error.response.data.errors.email){
                swal('Error!', error.response.data.errors.email[0], 'error')
            }else if(error.response.data.errors && error.response.data.errors.password){
                swal('Error!', error.response.data.errors.password[0], 'error')
            }else{
                swal('Error!', 'Email ou senha incorretos.', 'error')
            }
        });
    }

    return (
        <div id="page-login">
            <div className="content-wrapper">
                <img src={logoImg} alt=""/>

                <main>
                    <form onSubmit={handleSubmit} className="login-form">
                        <fieldset>
                            <legend>Efetue o login para continuar</legend>
                            {emailError !== '' && (
                                <p>{emailError}</p>
                            )}

                            <div className="input-block">
                                <label htmlFor="email">E-mail</label>
                                <input id="email" onChange={event => setEmail(event.target.value)}/>
                            </div>

                            <div className="input-block">
                                <label htmlFor="password">Senha</label>
                                <input id="password" type="password"
                                       onChange={event => setPassword(event.target.value)}/>
                            </div>

                            <button className="confirm-button" type="submit">
                                Entrar
                            </button>
                        </fieldset>
                    </form>
                </main>
            </div>
        </div>
    );
}
