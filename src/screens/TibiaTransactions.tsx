import React, {ChangeEvent, FormEvent, useState} from "react";
import '../styles/pages/TibiaTransaction.css';
import api from "../services/api";
import swal from 'sweetalert';
import {useHistory} from "react-router-dom";
import Sidebar from "../components/sidebar";
import PaypalForm from "../components/paypalForm";
import TransferWiseForm from "../components/transferWiseForm";
import Nubank from "../components/nubankForm";

import transferWise from '../images/transferwise.svg';
import nubank from '../images/nubank.svg';
import masterCard from '../images/mastercardlogo.png';

export default function CreateOrphanage() {
    const history = useHistory();


    const [paymentMethod, setPaymentMethod] = useState('');
    const [tibiacoins, setTibiaCoins] = useState();
    const [paymentLink, setPaymentLink] = useState('');
    const [billing, setBilling] = useState(true);

    let amount: number = 0;
    let items: object = [
        {
            "id": "1",
            "title": "Tibia Coins",
            "unit_price": 0o20,
            "quantity": tibiacoins,
            "tangible": true
        },
    ]

    // @ts-ignore
    if (tibiacoins > 0 && Number.isInteger(tibiacoins / 25)) {
        //@ts-ignore-block
        amount = tibiacoins * 0.20;
    } else { // @ts-ignore
        if (tibiacoins > 0) {
            swal("Erro!", "Selecione um valor valido! A quantidade minima e de 25 TC's.", "error");
        }
    }


    async function handleSubmit(event: FormEvent) {
        event.preventDefault();

        const data = new FormData();
        // @ts-ignore
        data.append('amount', amount);
        // @ts-ignore
        data.append('items', items);
        await api.post('pagarme/createTransaction', data).then(response => {
            console.log(response.data);
        });
    }

    return (
        <div id="page-create-orphanage">
            <Sidebar/>
            <main>
                <div className="create-orphanage-form">
                    <fieldset>
                        <legend>Quantidade: <i className='price'>Preco unitario(TC): R$0.20</i></legend>


                        <div className="buttons-tibiacoin">
                            <button className="quantity-button" type="button" onClick={event => setTibiaCoins(250)}>
                                250 TC's
                            </button>
                            <button className="quantity-button" type="button" onClick={event => setTibiaCoins(500)}>
                                500 TC's
                            </button>
                            <button className="quantity-button" type="button" onClick={event => setTibiaCoins(750)}>
                                750 TC's
                            </button>
                            <button className="quantity-button" type="button" onClick={event => setTibiaCoins(1000)}>
                                1000 TC's
                            </button>

                            <input id="quantity" placeholder="OUTRO VALOR"
                                   onBlur={event => setTibiaCoins(event.target.value)}/>
                        </div>


                        <legend>Escolha um metodo de pagamento:</legend>
                        {tibiacoins > 0 && Number.isInteger(tibiacoins / 25) && (
                            <div className="payment-methods-icons">

                                <button>
                                    <img src={masterCard} onClick={event => setPaymentMethod('masterCard')}/><label>Cartao
                                    de credito</label>
                                </button>

                                {/*<button>
                                    <img src={transferWise} onClick={event => setPaymentMethod('transferwise')}/>
                                </button>*/}

                                <button>
                                    <img src={nubank} onClick={event => setPaymentMethod('nubank')}/>
                                </button>
                            </div>
                        )}


                        {paymentMethod == 'masterCard' && (
                            <form onSubmit={handleSubmit}>
                                <fieldset>
                                    <legend>PagarMe</legend>

                                    <div className="input-block">
                                        <label htmlFor="total">Total a pagar</label>
                                        <input id="amount" value={'R$ ' + amount}/>
                                    </div>

                                    <div className="input-block">
                                        <label htmlFor="total">Link para pagamento</label>
                                        <input id="amount" value={paymentLink}/>
                                    </div>

                                    <div className="input-block">
                                        <label htmlFor="billing">Dados de cobranca e o mesmo do login?</label>
                                        <div className="button-select">
                                            <button type="button" className={billing ? 'active' : ''}
                                                    onClick={() => setBilling(true)}>Sim
                                            </button>
                                            <button type="button" className={!billing ? 'active' : ''}
                                                    onClick={() => setBilling(false)}>Não
                                            </button>
                                        </div>
                                    </div>
                                </fieldset>

                                <button className="confirm-button" type="submit">
                                    Confirmar
                                </button>
                            </form>
                        )}

                        {paymentMethod == 'paypal' && (
                            <PaypalForm/>
                        )}

                        {/*{paymentMethod == 'transferwise' && (
                            <TransferWiseForm/>
                        )}*/}

                        {paymentMethod == 'nubank' && (
                            <form onSubmit={handleSubmit}>
                                <fieldset>
                                    <legend>Nubank</legend>

                                    <div className="input-block">
                                        <label htmlFor="total">Total a pagar</label>
                                        <input id="amount" value={'R$ ' + amount}/>
                                    </div>

                                    <div className="input-block">
                                        <label htmlFor="total">Dados para transferencia:</label>
                                        <br/>
                                        <label>Agencia: 0001 </label>
                                        <label>Conta: 6151761-7 </label>
                                        <label>CPF: 439.032.268-07 </label>
                                        <br/>
                                        <label>Por favor coloque o nome do seu char na descricao da transacao, ou nos envie o comprovante
                                        no e-mail/whatsapp (fastcoins@nsystemsolutions.co.uk / +44 07375 926730</label>
                                    </div>

                                </fieldset>

                                <button className="confirm-button" type="submit">
                                    Confirmar
                                </button>
                            </form>
                        )}
                    </fieldset>
                </div>
            </main>
        </div>
    );
}
