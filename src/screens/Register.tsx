import React, {ChangeEvent, FormEvent, useState} from "react";
import InputMask from 'react-input-mask';
import '../styles/pages/register.css';
import api from "../services/api";
// @ts-ignore
import logoImg from "../images/logo.svg";
import swal from 'sweetalert';
import {useHistory} from "react-router-dom";
import Axios from "axios";


export default function Register() {
    const history = useHistory();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [name, setName] = useState();
    const [lastName, setLastName] = useState();
    const [phoneNumber, setPhoneNumber] = useState();
    const [charName, setCharName] = useState();
    const [postCode, setPostCode] = useState();
    const [street, setStreet] = useState('');
    const [country, setCountry] = useState();
    const [streetNumber, setStreetNumber] = useState();
    const [documentId, setDocumentId] = useState();
    const [birthday, setBirthday] = useState();
    const [charConfirmation, setCharConfirmation] = useState();

    if (typeof postCode == "string" && postCode.length > 0) {
        findCep();
    }

    async function findCep() {
        await Axios.get('https://viacep.com.br/ws/' + postCode + '/json/').then(resp => {
            setPostCode(resp.data);
            setStreet(resp.data.logradouro);
        }).catch(err => {
            swal('Erro!', 'CEP nao encontrado!', 'error')
        })
    }

    async function charFind(char: string) {
        await Axios.get('https://api.tibiadata.com/v2/characters/' + char + '.json').then((resp: any) => {
            // @ts-ignore
            swal('Confirme as informacoes',
                'Name: ' + resp.data.characters.data.name + '\n' +
                'World: ' + resp.data.characters.data.world + '\n' +
                'Level: ' + resp.data.characters.data.level + '\n' +
                'Residence: ' + resp.data.characters.data.residence + '\n\n' +
                'Caso as informacoes estejam corretas, prossiga com o cadastro :)'
                , 'success').then((option: boolean) => {
                setCharName(resp.data.characters.data.name);
                console.log(option);
            })
        }).catch(err => {
            swal('Erro!', err.message, 'error')
        })
    }

    async function handleSubmit(event: FormEvent) {
        event.preventDefault();
        const data = new FormData();

        data.append('email', email);
        data.append('password', password);
        data.append('name', name);
        data.append('last_name', lastName);
        data.append('phone_number', phoneNumber);
        data.append('char_name', charName);
        data.append('post_code', postCode.cep);
        data.append('street', postCode.logradouro);
        data.append('street_number', streetNumber);
        data.append('district', postCode.bairro);
        data.append('city', postCode.localidade);
        data.append('state', postCode.uf);
        data.append('country', 'Brazil');
        data.append('document_id', documentId);
        data.append('birthday', birthday);

        console.log(data)

        await api.post('register', {
            email,
            password,
            name,
            last_name: lastName,
            phone_number: phoneNumber,
            char_name: charName,
            post_code: postCode.cep,
            street: postCode.logradouro,
            street_number: streetNumber,
            district: postCode.bairro,
            city: postCode.localidade,
            state: postCode.uf,
            country: 'Brazil',
            document_id: documentId,
            birthday

        }, {
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((response: any) => {

            console.log('resposta: ' + response.data)
            //localStorage.setItem('token', response.data.token);

            history.push('/login')
        }).catch((error: any) => {

        });
    }

    return (
        <div id="page-register">
            <div className="content-wrapper">
                <img src={logoImg} alt=""/>

                <main>
                    <form onSubmit={handleSubmit} className="register-form">
                        <fieldset>
                            <legend>Efetue o login para continuar</legend>


                            <div className="input-block">
                                <label htmlFor="name">Nome</label>
                                <input id="name" required onChange={event => setName(event.target.value)}/>
                            </div>

                            <div className="input-block">
                                <label htmlFor="last_name">Sobrenome</label>
                                <input id="last_name" required onChange={event => setLastName(event.target.value)}/>
                            </div>

                            <div className="input-block">
                                <label htmlFor="last_name">Character name</label>
                                <input id="last_name" required onBlur={event => charFind(event.target.value)}/>
                            </div>

                            <div className="input-block">
                                <label htmlFor="email">E-mail</label>
                                <input id="email" required onChange={event => setEmail(event.target.value)}/>
                            </div>

                            <div className="input-block">
                                <label htmlFor="password">Senha</label>
                                <input id="password" required type="password"
                                       onChange={event => setPassword(event.target.value)}/>
                            </div>

                            <div className="input-block">
                                <label htmlFor="birthday">Data de nascimento</label>
                                <input id="birthday" required type="date"
                                       onChange={event => setBirthday(event.target.value)}/>
                            </div>

                            <div className="input-block">
                                <label htmlFor="cep">CEP</label>
                                <InputMask mask="99999999" required id="cep" onBlur={event => setPostCode(event.target.value)}/>
                            </div>

                            <div className="input-block">
                                <label htmlFor="rua">Rua</label>
                                <input id="rua" required value={street}
                                       onChange={event => setStreet(event.target.value)}/>
                            </div>

                            <div className="input-block">
                                <label htmlFor="rua">Numero</label>
                                <input id="rua" value={streetNumber}
                                       onChange={event => setStreetNumber(event.target.value)}/>
                            </div>

                            <div className="input-block">
                                <label htmlFor="rua">CPF</label>
                                <InputMask mask="99999999999" required id="cpf"
                                           onChange={event => setDocumentId(event.target.value)}/>
                            </div>

                            <div className="input-block">
                                <label htmlFor="rua">Numero de telefone</label>
                                <InputMask required mask="+55(**) * ********" id="cpf"
                                           onChange={event => setDocumentId(event.target.value)}/>
                            </div>

                            <button className="confirm-button" type="submit">
                                Entrar
                            </button>
                        </fieldset>
                    </form>
                </main>
            </div>
        </div>
    );
}
