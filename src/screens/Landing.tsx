import React from 'react';
import '../styles/global.css';
import '../styles/pages/landing.css'
import {FiArrowRight} from 'react-icons/fi'
import {Link} from 'react-router-dom';

import logoImg from '../images/logo.svg';
import picPay from '../images/ppay-icon.png';
import payPal from '../images/paypal-icon.svg';
import transferWise from '../images/transferwise.svg';
import nubank from '../images/nubank.svg';

function Landing() {
    return (
        <div id="page-landing">
            <div className="content-wrapper">
                <img src={logoImg} alt=""/>

                <main>
                    <h1>Sem burocracia, sem cadastros para compra!</h1>
                    <p>Metodos de pagamento: </p>
                    <a href="https://www.picpay.com/site" target="_blank">
                        <img src={picPay} alt=""/>
                    </a>

                    <a href="https://www.paypal.com/uk/home" target="_blank">
                        <img src={payPal} alt="" className="payPal"/>
                    </a>

                    <a href="https://transferwise.com/" target="_blank">
                        <img src={transferWise} alt="" className="payPal"/>
                    </a>

                    <a href="https://nubank.com.br/" target="_blank">
                        <img src={nubank} alt="" className="payPal"/>
                    </a>
                </main>

                <div className="location">
                    <strong>Status:</strong>
                    <span> Online</span>
                </div>
                <Link to="/buyTibiaCoins" className="enter-app">
                    <FiArrowRight size={26} color="rgba(8, 8 , 8, 8.6)"/>
                </Link>

            </div>
        </div>
    );
}

export default Landing;
