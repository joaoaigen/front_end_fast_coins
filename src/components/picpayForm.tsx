import React, {ChangeEvent, FormEvent, useState} from "react";
import { useHistory } from 'react-router-dom';
import api from "../services/api";
import swal from "sweetalert";


export default function PicpayForm() {
    const history = useHistory();

    const [paymentMethod, setPaymentMethod] = useState('');

    const [position, setPosition] = useState({latitude: 0, longitude: 0});
    const [name, setName] = useState('');
    const [about, setAbout] = useState('');
    const [instructions, setInstructions] = useState('');
    const [opening_hours, setOpeningHours] = useState('');
    const [open_on_weekends, setOpenOnWeekends] = useState(true);
    const [images, setImages] = useState<File[]>([]);
    const [previewImages, setPreviewImages] = useState<string[]>([]);

    async function handleSubmit(event: FormEvent) {
        event.preventDefault();
        const { latitude, longitude } = position;
        const data = new FormData();
        data.append('name', name);
        data.append('about', about);
        data.append('instructions', instructions);
        data.append('opening_hours', opening_hours);
        data.append('latitude', String(latitude));
        data.append('longitude', String(longitude));
        data.append('open_on_weekends', String(open_on_weekends));
        images.forEach(image => {
            data.append('images', image);
        })

        await api.post('orphanages', data);

        swal("Sucesso!", "Cadastro realizado com sucesso!");

        history.push('/app')
    }

    return (
        <form onSubmit={handleSubmit}>
            <fieldset>
                <legend>Picpay</legend>

                <div className="input-block">
                    <label htmlFor="name">Nome</label>
                    <input id="name" value={name} onChange={event => setName(event.target.value)}/>
                </div>

                <div className="input-block">
                    <label htmlFor="about">Sobre <span>Máximo de 300 caracteres</span></label>
                    <textarea id="name" maxLength={300} value={about} onChange={event => setAbout(event.target.value)}/>
                </div>

            </fieldset>

            <button className="confirm-button" type="submit">
                Confirmar
            </button>
        </form>
    );
}
