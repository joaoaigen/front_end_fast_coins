import React from "react";
import { BrowserRouter, Switch, Route} from 'react-router-dom';

import Landing from './screens/Landing';
import Login from './screens/Login';
import Register from './screens/Register';
import TibiaTransactions from './screens/TibiaTransactions';
import Orphanage from './screens/Orphanage';
import CreateOrphanage from './screens/CreateOrphanage';

function Routes() {
    return(
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={Landing} />
                <Route path="/login" component={Login} />
                <Route path="/register" component={Register} />
                <Route path="/buyTibiaCoins" component={TibiaTransactions} />
                <Route path="/orphanages/create" component={CreateOrphanage} />
                <Route path="/orphanages/:id" component={Orphanage} />
            </Switch>
        </BrowserRouter>
    );
}

export default Routes;
